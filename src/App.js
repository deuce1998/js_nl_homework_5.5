import React, {useState,useEffect} from "react"
import AccordionItem from './components/accordionItem';

function App() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(json => setUsers(json));
  }, []);

  return (
    <div className="App">
      {users.length > 0 &&  
        users.map((user, index) => (<AccordionItem key = {index} user = {user} />) )
      }
    </div>
  );
}

export default App;
