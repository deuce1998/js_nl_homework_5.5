import React, {useState,useEffect} from "react"
import './accordionItem.scss';
import './buttonAccordion.scss'

function AccordionItem({user}){
    
    const [active, setActive] = useState(false);

    return(
        <div className = "accordionItem">
            <button className={active ? "showAccordion active" : "showAccordion"} 
                onClick={() => {active ? setActive(false) : setActive(true); }}> 
                {user.name}
            </button>
            {active && 
                <div className = "accordionContent">
                    <p className="">username : {user.username}</p>
                    <p className="">email : {user.email}</p>
                    <p className="">phone : {user.phone}</p>
                    <p className="">website : {user.website}</p>
                </div>
            }
        </div>
    );
}

export default AccordionItem;